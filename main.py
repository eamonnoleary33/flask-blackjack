from flask import Flask, render_template, request, jsonify
import random

app = Flask(__name__)

def deal_initial_cards():
    user_cards = [deal_card() for _ in range(2)]
    computer_cards = [deal_card()]
    return user_cards, computer_cards

def deal_card():
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    return random.choice(cards)

def calculate_score(cards):
    score = sum(cards)
    if score > 21 and 11 in cards:
        cards.remove(11)
        cards.append(1)
        score = sum(cards)
    return score

def compare(user_score, computer_score):
    if user_score > 21 and computer_score > 21:
        return "Draw"
    elif user_score == computer_score:
        return "Draw"
    elif user_score == 0:
        return "User wins with a blackjack!"
    elif computer_score == 0:
        return "Computer wins with a blackjack!"
    elif user_score > 21:
        return "Computer wins. You went over 21."
    elif computer_score > 21:
        return "User wins. Computer went over 21."
    elif user_score > computer_score:
        return "User wins!"
    else:
        return "Computer wins!"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/play', methods=['POST'])
def play():
    user_cards, computer_cards = deal_initial_cards()
    user_score = calculate_score(user_cards)
    computer_score = calculate_score(computer_cards)
    if user_score == 21 or computer_score == 21:
        # If someone gets blackjack immediately, return result
        result = compare(user_score, computer_score)
        return jsonify({
            'user_cards': user_cards,
            'user_score': user_score,
            'computer_cards': computer_cards,
            'computer_score': computer_score,
            'result': result,
            'disable_hit': True,
            'disable_stand': True
        })
    else:
        return jsonify({
            'user_cards': user_cards,
            'user_score': user_score,
            'computer_cards': [computer_cards[0]],  # Retain only the initial card
            'computer_score': computer_score,
            'result': None,
            'disable_hit': False,
            'disable_stand': False
        })

@app.route('/hit', methods=['POST'])
def hit():
    user_cards = request.json.get('user_cards')
    user_cards.append(deal_card())
    user_score = calculate_score(user_cards)
    result = None
    disable_hit = False
    disable_stand = False

    if user_score <= 21:
        # If user has not exceeded 21, proceed with the game
        return jsonify({
            'user_cards': user_cards,
            'user_score': user_score,
            'result': result,
            'disable_hit': disable_hit,
            'disable_stand': disable_stand
        })

    # If user exceeds 21, proceed to let the computer draw its cards
    computer_cards = request.json.get('computer_cards', [])
    while calculate_score(computer_cards) < 17:
        computer_cards.append(deal_card())
    computer_score = calculate_score(computer_cards)
    # Evaluate the result
    result = compare(user_score, computer_score)
    disable_hit = True
    disable_stand = True

    return jsonify({
        'user_cards': user_cards,
        'user_score': user_score,
        'computer_cards': computer_cards,
        'computer_score': computer_score,
        'result': result,
        'disable_hit': disable_hit,
        'disable_stand': disable_stand
    })


@app.route('/stand', methods=['POST'])
def stand():
    user_cards = request.json.get('user_cards')
    computer_cards = request.json.get('computer_cards')
    user_score = calculate_score(user_cards)
    computer_score = calculate_score(computer_cards)
    while computer_score < 17:
        computer_cards.append(deal_card())
        computer_score = calculate_score(computer_cards)
    result = compare(user_score, computer_score)
    return jsonify({
        'user_score': user_score,
        'user_cards': user_cards,
        'computer_cards': computer_cards,
        'computer_score': computer_score,
        'result': result,
        'disable_hit': True,
        'disable_stand': True
    })

if __name__ == '__main__':
    app.run(debug=True)
